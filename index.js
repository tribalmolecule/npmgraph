const fs = require('fs');
const path = require('path');
const rimraf = require('rimraf');
const base64 = require('base-64');
const express = require('express');
const deepEqual = require('deep-equal');
const bodyParser = require('body-parser');
const parse = require('parse-yarn-lock').default;
const octokit = require('@octokit/rest')();
const { spawnSync } = require( 'child_process' );

const app = express();
const port = process.env.PORT || 8080;

app.use(bodyParser.urlencoded({ extended: true }));

// const repo = 'raceto2016';
// const owner = 'akshatsinha';
const pjPath = 'package.json';
const ref = 'master';

// function createNodeModules(owner, repo, dirName) {
//   octokit.repos.getContent({ owner, repo, path: pjPath, ref}, (err, result) =>  {
//     const packageJsonContent = base64.decode(result.data.content);
//     fs.writeFileSync(path.resolve(`./nm_dirs/${dirName}/package.json`), packageJsonContent, 'utf8', (err) => console.log(err));
//     // below: npm --prefix ./nm_dirs/dirname install ./nm_dirs/dirname --production
//     spawnSync('npm', ['--prefix', path.resolve(`./nm_dirs/${dirName}/`), 'install', path.resolve(`./nm_dirs/${dirName}/`), '--production']);
//     getDepMap();
//   });
// }

function doEverything(owner, repo) {
  const dirName = repo;
  return new Promise(function(resolve, reject) {
    if (!fs.existsSync(`./nm_dirs/${dirName}`)) {
      console.log(`Directory ${dirName} does not exist. Creating...`)
      fs.mkdirSync(path.resolve(`./nm_dirs/${dirName}`), 0766);
      octokit.repos.getContent({ owner, repo, path: pjPath, ref}, (err, result) =>  {
        const packageJsonContent = base64.decode(result.data.content);
        fs.writeFileSync(path.resolve(`./nm_dirs/${dirName}/package.json`), packageJsonContent, 'utf8', (err) => console.log(err));
        // below: npm --prefix ./nm_dirs/dirname install ./nm_dirs/dirname --production
        spawnSync('npm', ['--prefix', path.resolve(`./nm_dirs/${dirName}/`), 'install', path.resolve(`./nm_dirs/${dirName}/`), '--production']);
        resolve(getDepMap(dirName));
      })
    } else {
      console.log(`Directory ${dirName} found. Comparing current and previous package.json files...`);
      const existingPJData = JSON.parse(fs.readFileSync(path.resolve(`./nm_dirs/${dirName}/package.json`)))['dependencies'];
      octokit.repos.getContent({ owner, repo, path: pjPath, ref}, (err, result) =>  {
        const incomingPJData = JSON.parse(base64.decode(result.data.content))['dependencies'];
        if(deepEqual(existingPJData, incomingPJData) && fs.existsSync(`./nm_dirs/${dirName}/report.json`)) {
          const existingReport = JSON.parse(fs.readFileSync(`./nm_dirs/${dirName}/report.json`));
          console.log('Report found from previous search. Returning...')
          resolve(existingReport);
        } else {
          const packageJsonContent = base64.decode(result.data.content);
          fs.writeFileSync(path.resolve(`./nm_dirs/${dirName}/package.json`), packageJsonContent, 'utf8', (err) => console.log(err));
          // below: npm --prefix ./nm_dirs/dirname install ./nm_dirs/dirname --production
          spawnSync('npm', ['--prefix', path.resolve(`./nm_dirs/${dirName}/`), 'install', path.resolve(`./nm_dirs/${dirName}/`), '--production']);
          resolve(getDepMap(dirName));
        }
      })
    }
  })
}

function getDepMap(dirName) {
  let depMap = {}
  const packageJsonFirstLevelPackages = Object.keys(JSON.parse(fs.readFileSync(path.resolve(`./nm_dirs/${dirName}/package.json`)))['dependencies']);
  const packageLockDeps = JSON.parse(fs.readFileSync(path.resolve(`./nm_dirs/${dirName}/package-lock.json`)))['dependencies'];

  packageJsonFirstLevelPackages.forEach(mainpackage => {
    depMap[mainpackage] = {
      license: JSON.parse(fs.readFileSync(`./nm_dirs/${dirName}/node_modules/${mainpackage}/package.json`))['license'] || 'N/A',
      dependencies: {}
    }
    if (packageLockDeps[mainpackage] && packageLockDeps[mainpackage]['requires']) {
      secondLevelDeps = Object.keys(packageLockDeps[mainpackage]['requires']);
      secondLevelDeps.forEach(secondDep => {
        const license = JSON.parse(fs.readFileSync(`./nm_dirs/${dirName}/node_modules/${secondDep}/package.json`))['license'] || 'N/A';
        depMap[mainpackage]['dependencies'][secondDep] = license;
      })
    }
  });
  fs.writeFileSync(`./nm_dirs/${dirName}/report.json`, JSON.stringify(depMap), (err) => console.log('Could not generate report.json'));
  console.log('Generated report and returned.')
  return depMap;
}

app.post('/gitpath', (req, res) => {
  const url = req.body.gitpath;
  const urlParts = url.split('/');
  const repo = urlParts.pop();
  const user = urlParts.pop();
  doEverything(user, repo).then(result => res.json(result));
});

app.get('/', (req, res) => {
  res.sendFile(path.resolve('./index.html'));
})

app.listen(port);
console.log('Server started at http://localhost:' + port);
